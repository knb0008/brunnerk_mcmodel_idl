;@knb_mc_sim
;WRF Core profile: 8-23-2019, using mean level radii from DSD and conc from dry_air_density*qndrop/ice/etc
;Uses 1km average profile. Uncomment the levels in lambda if/then statements to use other averaging values.
;+
; Author: Kelcy Brunner, knb0008@uah.edu
;The purpose of this program is to simulate the randomized optical scattering occuring in the 777.4nm wavelength.
;-
pro knb_mc_sim , m, SEEDER = seeder,COORDS = coords, ARRAY_OUT = array_out,ORIGIN = origin,  PROFILE_IN = profile_in

;+
; Description:
;   This model ingests a number of photons (m), a source position, the shape of the cloud, the output array, and a microphysical profile.
;   All of which may be specified in the call line, or in a companion wrapper program title knb_mc_wrapper.pro
;
; Parameters:
;   m: value of the number of photons to be simulated
;        Type: int, or long int.
;
; Keywords
;   SEEDER: a seed for randomization. The default is systime(/sec).
;       Type: a single or double precision value, but a whole number is required by randomu.
;
;   COORDS: A 6 element array of the cloud coordinates of the faces in the following order: [x+, x-, y+, y-, z+, z-].
;   The units must be in the same units as the origin location, and if not specified is chosen as meters (m).
;       Type: Single or double precision is sufficient.
;
;   ARRAY_OUT: An array to recieve the end positions of the scattered photons.
;       Type: Empty single or double precision array.
;
;   ORIGIN: A 3 element array specifying the position of the photons to be simulated. The units must be the same as the COORDS keyword, the default is meters (m).
;       Type: Single or double precision is sufficient.
;
;   PROFILE_IN: This is the microphysical environment description. Importantly it will require the mean particle size and mean concentration for each layer (n) of the cloud. From which, the mean free path will be determined. The default cloud increment is 1km, and for a 10km cloud, n+1 layers will be needed. The first column must be the radius in meters, the second column must be the number concentration in #/m^3, and the third column must be the altitudes of each microphysical layer (preferably in meters). For simplicity the program is written for 20 1-km layers. This will be updated in future iterations.
;       Type: 3x20 array, double precision is preferred.
;
;
; Call example:
;   knb_mc_sim, 5000, seeder = systeim(/sec), COORDS = [10000d, 0d,10000d, 0d,12000d, 2000d], array_out = array_out, ORIGIN = [5d3,5d3,7d3], PROFILE_IN = profile
;
;
;
;-


  compile_opt idl2
  ;error catcher:
  CATCH, theError
  IF theError NE 0 THEN BEGIN
    CATCH, /cancel
    HELP, /LAST_MESSAGE, OUTPUT=errMsg
    FOR i=0, N_ELEMENTS(errMsg)-1 DO print, errMsg[i]
    RETURN
  ENDIF

  ;step 1: Photon is emitted in a random direction from the source location.
  
  ;step 2: Photon travels with an assumed probability that it travels a distance x without an interaction given by P(x) = exp(-x/LAM) where LAM is the mean free path.
  ;step 3: The photon 'encounters' a scatterer, it is either absorbed or scattered
  
  ;step 4: Check if photon is still within the cloud, if yes then it travels another distance x and step 3 is repeated or it exits the cloud.
  
  ;step 5: Check if photon is outside the cloud or absorbed, if it is, note the final photon position.
  
  ;step 6: repeat for another photon.


;Define the cloud.

;Define your boundaries and arrays to hold when a boundary is crossed.
s = size(coords)

Z_min = coords[5] ;this is the bottom of the cloud
Z_max = coords[4] ;this is the top of the cloud.
X_min = coords[1] ;this is the 1st face, the negative X-face
X_max = coords[0] ; this is the 2nd face, the positive X-face
Y_min = coords[3] ;this is the 3rd face, the negative Y-face
Y_max = coords[2] ;this is the 4th face, the positive Y-face

;;;;;;;;;CONSTANTS;;;;;;;;;
;Asymmetry factor for a 10micron water drop in the visible spectral region.
g = 0.87D0
;single scattering albedo for a water droplet of 10 microns in the visible spectral region.
w_0 = 0.99998d

;the radius of a typical cloud droplet
a = 10D-6
N =   1D8
LAM = 1D/(2D*!dpi*(a^2D)*N)

N_dense = profile_in[1,*]
a_arr = profile_in[0,*]

;;;Check microphysical profile
height = reform(profile_in[2,*])
if max(height) LE 100d then height = height*1d3

;Determine mean free path
;(m) mean free path of photons with a uniform population of drops.
LAM_ARR = reform(1d/(2d*!dpi*(a_arr^2d)*N_dense))





;calculate the array of possible scattering angles using the Henyey-Greenstein phase function
N = 2000D0
mu_arr = dblarr(N)
mu_arr[0] = -1D ;formerly 0D

for i=1,n_elements(mu_arr)-1D do begin
  mu_arr[i] = ((1D + g^2D)-((2D*g)/(N*(1D - g^2D)) + (1D + g^2D - 2D*g*mu_arr[i-1])^(-1D/2D))^(-2D))/(2D*g)
    if mu_arr[i] GT 1D then mu_arr[i] = 1D 
endfor
 pp = indgen(n_elements(mu_arr));*********

 x_arr = dblarr(m)
 y_arr = dblarr(m)
 z_arr = dblarr(m)
 absorb = 0d

;location = []
 
 theta_orig = dblarr(m)
 phi_orig = dblarr(m)
 seed1 = seeder
 seed2 = seed1 + 1d
 seed3 = seed2 + 2d
 seed4 = seed3 + 3d
 seed5 = seed4 + 4d
 seed6 = seed5 + 5d

 u = randomu(seed1,m)
 v = randomu(seed2,m) 

ab_arr_m = randomu(seed5, m)
r_m = randomu(seed3,m)
;initial photon isotropically scattered directions
theta_0 = acos(2D*u - 1D)
phi_0 = 2d*!dpi*v
if origin[2] GE 0d  and origin[2] LT height[0] then LAM = LAM_arr[0]
if origin[2] GE height[0]  and origin[2] LT height[1] then LAM = LAM_arr[1]
if origin[2] GE height[1]  and origin[2] LT height[2] then LAM = LAM_arr[2]
if origin[2] GE height[2]  and origin[2] LT height[3] then LAM = LAM_arr[3]
if origin[2] GE height[3]  and origin[2] LT height[4] then LAM = LAM_arr[4]
if origin[2] GE height[4]  and origin[2] LT height[5] then LAM = LAM_arr[5]
if origin[2] GE height[5]  and origin[2] LT height[6] then LAM = LAM_arr[6]
if origin[2] GE height[6]  and origin[2] LT height[7] then LAM = LAM_arr[7]
if origin[2] GE height[7]  and origin[2] LT height[8] then LAM = LAM_arr[8]
if origin[2] GE height[8]  and origin[2] LT height[9] then LAM = LAM_arr[9]
if origin[2] GE height[9]  and origin[2] LT height[10] then LAM = LAM_arr[10]
if origin[2] GE height[10]  and origin[2] LT height[11] then LAM = LAM_arr[11]
if origin[2] GE height[11]  and origin[2] LT height[12] then LAM = LAM_arr[12]
if origin[2] GE height[12]  and origin[2] LT height[13] then LAM = LAM_arr[13]
if origin[2] GE height[13]  and origin[2] LT height[14] then LAM = LAM_arr[14]
if origin[2] GE height[14]  and origin[2] LT height[15] then LAM = LAM_arr[15]
if origin[2] GE height[15]  and origin[2] LT height[16] then LAM = LAM_arr[16]
if origin[2] GE height[16]  and origin[2] LT height[17] then LAM = LAM_arr[17]
if origin[2] GE height[17]  and origin[2] LT height[18] then LAM = LAM_arr[18]
if origin[2] GE height[18]  and origin[2] LT height[19] then LAM = LAM_arr[19]


x_scat_0 = -LAM*ALOG(r_m)
ind_m = randomu(seed4, m)

theta_ind_m_seed = randomu(seed6,m)
phi_r_m = randomu(seed2,m)
r_m_1 = randomu(seed3,m)

for j= 0, m - 1D do begin
  bound = 0D
  k=0D
  ab_arr = randomu(ab_arr_m[j]*100d, 2000000)


;The position of the particle, after being emitted isotropically and scattered a distance x_scat_0 is given by:
;direction cosines
mu_x = sin(theta_0[j])*cos(phi_0[j])
mu_y = sin(theta_0[j])*sin(phi_0[j])
mu_z = cos(theta_0[j])

;positions
x = origin[0] + x_scat_0[j]*mu_x
y = origin[1] + x_scat_0[j]*mu_y
z = origin[2] + x_scat_0[j]*mu_z
;location = [[location],[x,y,z]]

IF ~logical_and(theta_0[j], phi_0[j])  then begin
;This is the 1st scatter calculations - for testing purposes
;The position of the particle, after being emitted isotropically and scattered a distance x_scat_0 is given by:
;direction cosines
mu_x = sin(theta_0[j])*cos(phi_0[j])
mu_y = sin(theta_0[j])*sin(phi_0[j])
mu_z = cos(theta_0[j])

;positions
x = x + x_scat_0[j]*mu_x
y = y + x_scat_0[j]*mu_y
z = z + x_scat_0[j]*mu_z
endif 

;Find angles of first scattering, these are relative to incoming angle from isotropic scatter.
ind = randomu(ind_m[j]*100, 2000)*1999d + 1d



theta_arr = acos((mu_arr[pp[1:*]] + mu_arr)/2d) ; this gives us most angles near 0



;*****
theta_ind = round(randomu(theta_ind_m_seed[j]*100d,500000)*2000) ;this may produce the same indicie each time.
theta_p = theta_arr[theta_ind]
phi_p = 2D*!dpi*randomu(phi_r_m[j]*100d,500000)
r = randomu(r_m_1[j]*100d,500000)
x_scat = -ALOG(r)
;******
tic
; Get current allocation and reset the high water mark:
start_mem = MEMORY(/CURRENT)
repeat begin

;check to see if absorbed
if ab_arr[k] GT w_0 then absorb += 1d
if ab_arr[k] GT w_0 then break

;Find the photon's second position
;direction cosines
denom = sqrt(1d - mu_z^2D)
mu_x_new = ((sin(theta_p[k])*(mu_x*mu_z*cos(phi_p[k]) - mu_y*sin(phi_p[k])))/denom) + mu_x*cos(theta_p[k])
mu_y_new = ((sin(theta_p[k])*(mu_y*mu_z*cos(phi_p[k]) + mu_x*sin(phi_p[k])))/denom) + mu_y*cos(theta_p[k])
mu_z_new = -(denom)*sin(theta_p[k])*cos(phi_p[k]) + mu_z*cos(theta_p[k])

if z GE 0d  and z LT height[0] then LAM = LAM_arr[0]
if z GE height[0]  and z LT height[1] then LAM = LAM_arr[1]
if z GE height[1]  and z LT height[2] then LAM = LAM_arr[2]
if z GE height[2]  and z LT height[3] then LAM = LAM_arr[3]
if z GE height[3]  and z LT height[4] then LAM = LAM_arr[4]
if z GE height[4]  and z LT height[5] then LAM = LAM_arr[5]
if z GE height[5]  and z LT height[6] then LAM = LAM_arr[6]
if z GE height[6]  and z LT height[7] then LAM = LAM_arr[7]
if z GE height[7]  and z LT height[8] then LAM = LAM_arr[8]
if z GE height[8]  and z LT height[9] then LAM = LAM_arr[9]
if z GE height[9]  and z LT height[10] then LAM = LAM_arr[10]
if z GE height[10]  and z LT height[11] then LAM = LAM_arr[11]
if z GE height[11]  and z LT height[12] then LAM = LAM_arr[12]
if z GE height[12]  and z LT height[13] then LAM = LAM_arr[13]
if z GE height[13]  and z LT height[14] then LAM = LAM_arr[14]
if z GE height[14]  and z LT height[15] then LAM = LAM_arr[15]
if z GE height[15]  and z LT height[16] then LAM = LAM_arr[16]
if z GE height[16]  and z LT height[17] then LAM = LAM_arr[17]
if z GE height[17]  and z LT height[18] then LAM = LAM_arr[18]
if z GE height[18]  and z LT height[19] then LAM = LAM_arr[19]

x = x + LAM*x_scat[k]*mu_x_new
y = y + LAM*x_scat[k]*mu_y_new
z = z + LAM*x_scat[k]*mu_z_new
mu_x = mu_x_new
mu_y = mu_y_new
mu_z = mu_z_new
;location = [[location],[x,y,z]]
k = k+1D
;check boundary conditions
if origin[2] GE coords[5] then begin ;if the original point is 'inside the cloud'
if z GT z_max then bound = 1d
if z LE z_min then bound = 1d
if y GT y_max then bound = 1d
if y LE y_min then bound = 1d
if x GT x_max then bound = 1d
if x LE x_min then bound = 1d
endif else begin ;if the original point is outside the cloud
if z GT z_max then bound = 1d
;if z LE z_min then bound = 1d
if z LE 0L then bound = 1d ;it's below the ground
if y GT y_max then bound = 1d
if y LE y_min then bound = 1d
if x GT x_max then bound = 1d
if x LE x_min then bound = 1d
endelse
endrep until bound GT 0D 

x_face = 0
y_face = 0
z_face = 0
;arr_mid = [[arr_mid],[x],[y],[z]]

x_arr[j] = x
y_arr[j] = y
z_arr[j] = z

endfor

x_face = 0
y_face = 0
z_face = 0
array_out = transpose([[x_arr],[y_arr],[z_arr]])

print, absorb
toc
end
