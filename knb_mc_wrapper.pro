;@knb_mc_wrapper

pro knb_mc_wrapper;
compile_opt idl2
;error catcher:
CATCH, theError
IF theError NE 0 THEN BEGIN
  CATCH, /cancel
  HELP, /LAST_MESSAGE, OUTPUT=errMsg
  FOR i=0, N_ELEMENTS(errMsg)-1 DO print, errMsg[i]
  RETURN, -1
ENDIF

;The profile used in simulating the microphysical environment
;The default is the homogeneous profile. Additional profiles may be substituted.
restore, 'homo_profile.sav'


;The origin array is the point location of the source. A point source is the default, however an array may be restored and iterated through. A sample call for the array is commented, and again, the default is a point souce and must be commented out if an array is used. Note, the units of origin array must be in the same units as the coordinates, in this case it is meters (m).
;restore, 'origin_array.sav'
origin_array = [5d3,5d3,7d3] ;the position is the center of the cloud

;the coordinates of the simulated cloud
Coords = [10000d, 0d,10000d, 0d,12000d, 2000d]

;Selecting the number of frames to be simulated over. The default is 1, but this may be modified if a new origin or array of origin points is introduced for each frame. This is largely beneficial for simulating a full lightning flash, as opposed to a single frame or point source. The frame may be as long as you wish, the frame time only becomes pertinent when creating the source points. I select (prior to the simulation) the source points falling within a GLM frame. A frame is arbitrary in this model, and you may simulate all the sources in one frame, or wish to step through and save individual files for each frame - which may be easier for post processing. The choice is yours!
no_frames = 1
;path to dir
;path = ;string of path name here
;no_frames = filesearch(path,count = no_frames)


;;;;selecting the number of photons and the seeds for this simulation;;;;;
;1. Set the number of photons to simulate
m = 5000d ;the number of photons to simulate

;2. Set the size of the seed array. This is dependant on the number of source points
s = size(origin_array)
seed_time = double(systime(/sec)) ;this may be fixed if desired
seed = randomu(seed_time)*100d(dindgen(no_frames,s[2]))
save, seed, filename = 'seed.sav'


;;Begin iterating over each frame in the flash, and further over each source point in each flash. The default is a single frame, but the structure is left in this wrapper program should you desire to process multiple frames. , feeding the cloud dimensions, source points, number photons, and microphysical profile into the Monte Carlo Model.

for k = 0, no_frames - 1 do begin
array_large = [ ] ;array large is the final location of the photon.

;;iterating over the number of sources in a frame
    for i = 0, s[2] - 1d do begin

;;Following program is to call the model, the inputs are the number of photons, seeds, coordinates of the cloud, and output array, an input source array, and the input microphysics.
;;
        knb_mc_sim, m, seeder = seed[k,i], COORDS = coords, array_out = array_out, ORIGIN = origin_array[*,i], PROFILE_IN = profile


;compiling the consecutive arrays of photons should be there be multiple frames in a simulation.
        array_large = [[array_large], [array_out]]

        endfor

;the file name selected here is the frame number_source origin. 
save, array_large, filename = strcompress('array_large'+string(k)+'_'+string(round(mean(origin_array[2,*]/1d3)))+'.sav',/remove_all)
endfor

end
